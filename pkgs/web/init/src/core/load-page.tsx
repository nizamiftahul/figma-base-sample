import { matchRoute } from 'libs'
import type { BaseWindow } from '../window'

declare const window: BaseWindow

export const findPage = (url: string) => {
  if (!window.cms_pages) {
    window.cms_pages = {}
    console.warn('[BASE] No Pages Loaded.');
  }

  for (let [k, v] of Object.entries(window.cms_pages)) {
    const params = matchRoute(url, k)

    if (params) {
      return { url: k, ...v, params }
    }
  }
  return false
}

export const loadPage = async (found: ReturnType<typeof findPage>) => {
  if (found) {
    let source = window.cms_pages[found.url].source
    if (!source) {
      const ssr = found.ssr
      if (!window.cms_init && ssr) {
        /**
         * kodingan dibawah ini buat optimize load saat pertama kali ssr page.
         *
         * Kalau cms_init itu false berarti
         * halaman ini kebuka pertama kali (bukan lewat <a href/> sebelumnya)
         * jadi kita harus kirim params ke server supaya
         * server_on_load ga ke-eksekusi 2x
         *    (di server di cek ketika ada params, ga eksekusi server_onload)
         * yaitu:
         *   - saat ke initial load (refresh pertama, eksekusi server_on_load)
         *   - saat ajax page.cpx
         *       (nah disini ga perlu server_on_load lagi,
         *        reuse dari params aja, hasil dari server_on_load yg pertama )
         */
        source = await fetchPageById(found.id, {
          ssr: true,
        })
      } else {
        // ga perlu kirim params, meskipun dia ssr.
        // karena jika masuk ke-sini otomatis bukan load pertama kali
        // sehingga eksekusi server_on_load aja saat fetch,
        // dan hasil params dari server_on_load tsb digunakan untuk render page
        source = await fetchPageById(found.id, { ssr: ssr })
      }

      if (!ssr) {
        found.source = source
        window.cms_pages[found.url].source = source
      }
    }
    return found
  }
  return false
}

export function fetchPageById(
  id: string,
  opt: {
    ssr: boolean
    params?: any
  }
): Promise<string> {
  return new Promise((resolve) => {
    function failureListener(err) {
      console.log('Request failed', err)
    }

    var request = new XMLHttpRequest()
    request.onload = function (this: any) {
      resolve(this.responseText)
    }

    request.onerror = failureListener
    request.open('post', `/__cms/${id}/${opt.ssr ? 'ssr' : 'page'}.csx`, true)
    request.setRequestHeader('x-cpx-request', 'yes')
    if (opt.params) {
      request.setRequestHeader('Content-Type', 'application/json;charset=UTF-8')
      request.setRequestHeader('x-cpx-params', 'yes')
      request.send(JSON.stringify(opt.params))
    } else {
      request.send()
    }
  })
}
