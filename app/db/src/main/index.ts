import { PrismaClient, Prisma } from '@prisma/client'
export const main = new PrismaClient()

export type m_warehouse = {
      m_warehouse_id: Prisma.Decimal
      ad_client_id: Prisma.Decimal
      ad_org_id: Prisma.Decimal
      isactive: string
      created: Date
      createdby: Prisma.Decimal
      updated: Date
      updatedby: Prisma.Decimal
      value: string
      name: string
      description: string | null
      c_location_id: Prisma.Decimal
      separator: string
      m_warehousesource_id: Prisma.Decimal | null
      replenishmentclass: string | null
      isintransit: string | null
      isdisallownegativeinv: string
      m_warehouse_uu: string | null
      m_reservelocator_id: Prisma.Decimal | null
      l_location: Prisma.JsonValue | null
      m_area: m_area[]
}

export type m_area = {
      ad_client_id: Prisma.Decimal | null
      ad_org_id: Prisma.Decimal | null
      created: Date | null
      createdby: Prisma.Decimal | null
      description: string | null
      isactive: string | null
      m_area_id: Prisma.Decimal
      m_area_uu: string | null
      m_warehouse_id: Prisma.Decimal
      name: string | null
      updated: Date | null
      updatedby: Prisma.Decimal | null
      value: string | null
      l_location: Prisma.JsonValue | null
      m_locator: m_locator[]
}

export type m_locator = {
      m_locator_id: Prisma.Decimal
      ad_client_id: Prisma.Decimal
      ad_org_id: Prisma.Decimal
      isactive: string
      created: Date
      createdby: Prisma.Decimal
      updated: Date
      updatedby: Prisma.Decimal
      value: string
      m_warehouse_id: Prisma.Decimal
      priorityno: Prisma.Decimal
      isdefault: string
      x: string | null
      y: string | null
      z: string | null
      m_locator_uu: string | null
      m_locatortype_id: Prisma.Decimal | null
      m_area_id: Prisma.Decimal | null
      l_location: Prisma.JsonValue | null
}
