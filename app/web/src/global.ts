import dateFormat from 'date-fns/format'
import parseISO from 'date-fns/parseISO'

export const money = (angka: string | number) => {
  let rupiah = ''

  if (!angka) return '-'
  const angkarev = (angka ).toString().split('').reverse().join('')
  for (var i = 0; i < angkarev.length; i++)
    if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.'
  return (
    'Rp. ' +
    rupiah
      .split('', rupiah.length - 1)
      .reverse()
      .join('')
  )
}

const epochs = [
  ['year', 31536000],
  ['month', 2592000],
  ['day', 86400],
  ['hour', 3600],
  ['min', 60],
  ['sec', 1],
]

export const timeAgo = (date: any, ender = 'ago') => {

  if (!date) return '-';
  
  const timeAgoInSeconds = Math.floor(
    ((new Date() as any) - (new Date(date) as any)) / 1000
  ) as any

  const { interval, epoch } = getDuration(
    timeAgoInSeconds < 0 ? 1 : timeAgoInSeconds
  ) as any
  const suffix = interval === 1 ? '' : ''

  return `${interval} ${epoch}${suffix} ${ender}`
}

export const date = (date: string | Date, format = 'dd MMM yyyy') => {
  return dateFormat(
    typeof date === 'string' ? parseISO(date) : date,
    format
  )
}

const getDuration = (timeAgoInSeconds) => {
  for (let [name, seconds] of epochs as any) {
    const interval = Math.floor(timeAgoInSeconds / seconds)

    if (interval >= 1) {
      return {
        interval: interval,
        epoch: name,
      }
    }
  }
  return {
    interval: 1,
    epoch: 'Beberapa saat',
  }
}