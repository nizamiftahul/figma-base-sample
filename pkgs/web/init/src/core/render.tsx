/** @jsx jsx */
import F7Page from 'framework7-react/esm/components/page'
import React, { Fragment, useEffect, useRef } from 'react'
import { useRender } from 'web.utils/src/useRender'
import { matchRoute } from 'libs'
import type { BaseWindow } from '../window'
import { generatePage } from './gen-page'
import { findPage, loadPage } from './load-page'

declare const window: BaseWindow
export const renderCore = ({ url }: { url: string }) => {
  const _ = useRef({
    page: findPage(url),
    cache: null as null | ReturnType<typeof generatePage>,
    params: getUrlParams(url),
    mounted: true,
  })
  const internal = _.current

  useEffect(() => {
    return () => {
      internal.mounted = false
    }
  }, [])

  const render = useRender()
  let Layout: React.FC<any> = ({ children }: any) => {
    const Wrapper = window.platform === 'mobile' ? F7Page : Fragment
    return <Wrapper>{children}</Wrapper>
  }

  if (typeof internal.page === 'object') {
    const layout = window.cms_layouts[internal.page.layout_id]
    if (layout) {
      Layout = layout.component // swap empty layout with correct layout
    }

    // render cuman yg ga pakai SSR aja
    // todo: kerjain yg pake SSR plis..
    if (!internal.page.ssr) {
      const layoutArgs = {}
      layoutArgs['params'] = internal.params

      if (internal.page.source) {
        if (!internal.cache) {
          internal.cache = generatePage(internal.page.source, {
            params: { ...internal.params, url },
            updateParams: (newparams) => {
              internal.params = newparams
            },
          })
        }

        const Page = internal.cache

        return (
          <Layout {...layoutArgs}>
            <ErrorBoundary>
              <Page />
            </ErrorBoundary>
          </Layout>
        )
      } else {
        internal.cache = null
        loadPage(internal.page).then(() => {
          render()
          window.cms_init = true
        })
        return <Layout {...layoutArgs} />
      }
    }
  }
  console.warn('[BASE] Page not found')
  return null // 404 not found
}

export const getUrlParams = (url: string) => {
  const route = Object.keys(window.cms_pages)
    .map((e) => ({ route: matchRoute(url, e) }))
    .filter((e) => e.route)

  if (route.length > 0) {
    return { ...route[0].route, url }
  }
  return {}
}

class ErrorBoundary extends React.Component<any, any> {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true })
  }

  render() {
    if (this.state.hasError) {
      return null
    }
    return this.props.children
  }
}
