// make sure to export default component not export const
export default {
  "render-html": () => [
    import("web.utils/components/RenderHTML"),
    { c: "", s: "", h: "" },
  ],
  "html-head": () => [
    import("web.utils/components/HtmlHead"),
    { c: "", s: "", h: "" },
  ],
  "hello-world": () => [
    import("web.utils/components/HelloWorld"),
    { c: "", s: "", h: "" },
  ],
  loading: () => [import("web.form/fields/Loading"), { c: "", s: "", h: "" }],
  admin: () => [import("web.form/src/AdminCMS"), { c: "", s: "", h: "" }],
  qform: () => [import("web.form/src/QForm"), { c: "", s: "", h: "" }],
  qlist: () => [import("web.list/src/QList"), { c: "", s: "", h: "" }],
  primary: () => [import("./components/primary"), { c: "", s: "", h: "" }],
};
